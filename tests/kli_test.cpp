#include <gtest/gtest.h>
#include <atomic>
using namespace std;

extern "C" {
    #include <kli.h>
    #include <malloc.h>
}
int test_kli_show_test(kli_t *cli, void *context, kli_exec_t *exec)
{
	return 0;
}
int test_kli_show_tests(kli_t *cli, void *context, kli_exec_t *exec)
{
	return 0;
}
int test_kli_show_testwith_args(kli_t* cli, void *context, kli_exec_t *exec)
{
	return 0;
}

kli_t _cli = { 0 };
kli_t *cli = &_cli;
kli_com_t *show = 0;
kli_com_t *show_test = 0;
kli_com_t *show_test2 = 0;
kli_com_t *show_testwithargs = 0;
void setup_cli()
{
	kli_init(cli);
	show = kli_register_command_allocs(cli, 0, "show", "show help", 0, NULL);
	show_test = kli_register_command_allocs(cli, show, "test", "show test help", test_kli_show_test, NULL);
	show_test2 = kli_register_command_allocs(cli, show, "tests", "show tests help", test_kli_show_tests, NULL);
	show_testwithargs = kli_register_command_allocs(cli, show, "testswithargs", "show testswithargs help", test_kli_show_testwith_args, NULL);
}

TEST(kli, kli_prompt_allocs) {

}

TEST(kli, kli_process_line) {

}

TEST(kli, kli_recurse_print_commands) {
	setup_cli();
	kli_recurse_print_commands(cli,  &cli->root_command, 0);
	kli_release_command_frees(cli, &cli->root_command);
}

TEST(kli_register_command, register){
	kli_init(cli);

	int context = 0x00010203;
	kli_com_t *com = kli_register_command_allocs(cli, 0, "show", "show help", 0, &context);

	ASSERT_EQ(1, cli->root_command.child_list.count);
	kli_com_t *child = (kli_com_t*)cli->root_command.child_list.head->value;
	ASSERT_TRUE(child != NULL);
	ASSERT_TRUE(child == com);
	ASSERT_TRUE(strcmp("show", child->command) == 0);
	ASSERT_TRUE(strcmp("show help", child->help) == 0);
	kli_release_command_frees(cli, &cli->root_command);
}

TEST(kli_register_command, register_cb){
	kli_init(cli);

	int context = 0x00010203;
	kli_com_t *com = kli_register_command_allocs(cli, NULL, "test", "show test help", test_kli_show_test, &context);

	ASSERT_EQ(1, cli->root_command.child_list.count);
	kli_com_t *child = (kli_com_t*)cli->root_command.child_list.head->value;
	ASSERT_TRUE(child != NULL);
	ASSERT_TRUE(child == com);
	ASSERT_EQ(test_kli_show_test, child->action.func);
	ASSERT_EQ(&context, (int*)child->action.context);
	ASSERT_TRUE(strcmp("test", child->command) == 0);
	ASSERT_TRUE(strcmp("show test help", child->help) == 0);
	kli_release_command_frees(cli, &cli->root_command);
}

TEST(kli_register_command, child_commands){
	kli_init(cli);

	show = kli_register_command_allocs(cli, 0, "show", "show help", 0, NULL);
	show_test = kli_register_command_allocs(cli, show, "test", "show test help", test_kli_show_test, NULL);

	ASSERT_EQ(1, cli->root_command.child_list.count);
	kli_com_t *child = (kli_com_t*)cli->root_command.child_list.head->value;
	ASSERT_TRUE(strcmp("show", child->command) == 0);
	ASSERT_TRUE(strcmp("show help", child->help) == 0);


	ASSERT_EQ(1, child->child_list.count);
	ASSERT_TRUE(child->child_list.head != 0);
	ASSERT_TRUE(show_test == child->child_list.head->value);

	kli_release_command_frees(cli, &cli->root_command);

}

TEST(kli_get_matching_child_command, test){
	setup_cli();
	kli_com_t *command = kli_get_matching_child_command((char*)"tests", (kli_com_t*)cli->root_command.child_list.head->value);
	ASSERT_TRUE(test_kli_show_tests == command->action.func);

	command = kli_get_matching_child_command((char*)"invalid", (kli_com_t*)cli->root_command.child_list.head->value);
	ASSERT_TRUE(0 == command);
	kli_release_command_frees(cli, &cli->root_command);
}

TEST(kli_parse_line, parse_invalid_command){
	setup_cli();
	char *line = strdup("invalid");
	kli_exec_t exec = { 0 };
	kli_com_t *com = kli_parse_line_allocs(cli, &exec, line, NULL);
	ASSERT_TRUE(&cli->root_command == com);
	ASSERT_TRUE(0 == exec.command);
	free(line);
	kli_release_exec_frees(&exec);
	kli_release_command_frees(cli, &cli->root_command);
}


TEST(kli_parse_line, parse_valid_partial_command){
	setup_cli();

	char *line = strdup("show");
	kli_exec_t exec = { 0 };
	kli_com_t *com = kli_parse_line_allocs(cli, &exec, line, NULL);
	ASSERT_TRUE(show == com);
	ASSERT_TRUE(0 == exec.command);
	free(line);
	kli_release_exec_frees(&exec);

	line = strdup("show test arg1 arg2");
	exec = { 0 };
	com = kli_parse_line_allocs(cli, &exec, line, NULL);
	ASSERT_TRUE(show_test == com);
	ASSERT_TRUE(show_test == exec.command);
	ASSERT_EQ(2, exec.argc);
	ASSERT_TRUE(!strcmp("arg1", exec.argv[0]));
	ASSERT_TRUE(!strcmp("arg2", exec.argv[1]));
	free(line);
	kli_release_exec_frees(&exec);
	kli_release_command_frees(cli, &cli->root_command);
}

TEST(kli, kli_process_thread_input){

}
TEST(kli, kli_thread) {

}
TEST(klik, kli_shutdown_initialized) {
	setup_cli();
	kli_release_command_frees(cli, &cli->root_command);
	kli_shutdown(cli);
}

TEST(klik, kli_shutdown_zeroed) {
	bzero(cli, sizeof(kli_t));
	kli_shutdown(cli);
}

TEST(kli, kli_start_input_thread) {

}
