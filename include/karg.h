/**
 * @file karg.h
 * @author Jason Kushmaul
 * @date 9 Sep 2017
 * @brief Small quick and easy command argument processing
 * @see https://klib.gitlab.io/libkli/
 */
#ifndef KARG_H
#define KARG_H
#include <stddef.h>
#include <stdarg.h>
#include <stdlib.h>
#include <klist.h>
#include <getopt.h>

/*
 * I wanted to make an easier to use interface to getopt.  It's still getopt
 * but a little more natural.
 *
 * I guess the closest match would be argtable, but that's BSD and I wanted to produce gpl library
 *
 *
 * Git just does it themselves:
 * Just a huge if-elseif while loop
 * https://github.com/git/git/blob/master/parse-options-cb.c
 * https://github.com/git/git/blob/master/git.c#L46
 * It looks like if the git specific could be removed this would replace getopt (someone get on that)
 *
 * GPG rolled their own "argparse" https://github.com/gpg/gnupg/blob/master/common/argparse.c
 *
 * I'd like to have option groups, where you can say the group is required, meaning one of the options must be present.
 *
 * I think eventually it would be easy to add option groups to options creating required options for optional args.
 * Like if you specify "--encrypt", then you must also specify "--key-id", something like that.
 *      This would have an optional arg "--encrypt", with a child required argument "--key-id" - it's only required when --encrypt is found.
 *
 *
 */

typedef struct karg_t karg_t;
typedef struct karg_option_t karg_option_t;
typedef struct karg_command_t karg_command_t;


typedef int (*karg_option_callback_f)(karg_t *args, karg_option_t *option, char *argv);
typedef int (*karg_command_callback_f)(karg_t *args, karg_command_t *cmd, char *argv);

#define KARG_LONG_OPTION_LEN 32
#define KARG_HELP_LEN 512

/**
 * @brief Structure containing option definition
 *
 */
struct karg_option_t {
	char short_option;
	char long_option[KARG_LONG_OPTION_LEN];
	size_t long_option_len;
	char help[KARG_HELP_LEN];
	karg_option_callback_f callback;

	int argv_index;
	int required;
	int takes_value;
	void *value_len;
	void *value;
};

#define KARG_MAX_OPTIONS 256
typedef struct karg_getopt_option_def {
	size_t optlen;
	char short_options[KARG_MAX_OPTIONS];
	struct option long_options[0];
} karg_getopt_option_def;


#define KARG_COMMAND_LEN 32
/**
 * @brief Structure containing command definition
 *
 */
struct karg_command_t {
	int argv_index;
	char command[KARG_COMMAND_LEN];
	char help[KARG_HELP_LEN];
	klist_t options;
	karg_command_callback_f callback;
};

#define KARG_PROGNAME_LEN 255
#define KARG_VERSION_LEN 255
#define KARG_SUMMARY_LEN 512
#define KARG_AUTHOR_LEN 255


typedef struct karg_error_t {
	int has_error;
	char error_msg[1024];
} karg_error_t;
/**
 * @brief Structure containing argument definitions
 *
 */
struct karg_t {
	char program_name[KARG_PROGNAME_LEN];
	char version[KARG_VERSION_LEN];
	char author[KARG_AUTHOR_LEN];
	klist_t options;
	klist_t commands;
	void *context;
	karg_error_t errors;
};

/**
 * @brief Just zeroes memory for now
 *
 * @param args p_args:...
 */
void karg_init(karg_t *args);

/**
 * @brief Releases global options, commands' options, and commands lists.
 *
 * @param args p_args:...
 */
void karg_release_frees(karg_t *args);

/**
 * @brief Zeroes and copies given values to given structure
 *
 * @param options p_options:The destination list of karg_option_t
 * @param option p_option:The destination karg_option_t
 * @param short_opt p_short_opt:...
 * @param long_opt p_long_opt:...
 * @param help p_help:...
 * @param required p_required:...
 * @param callback p_callback:...
 * @param takes_value p_takes_value:...
 */
void karg_option_init(klist_t *options, karg_option_t *option, char short_opt, char *long_opt, char *help, int required, karg_option_callback_f callback, int takes_value);

/**
 * @brief Zeroes and copies given values to given structure
 *
 * @param commands p_commands:...
 * @param cmd p_cmd:...
 * @param name p_name:...
 * @param help p_help:...
 * @param callback p_callback:...
 */
void karg_command_init(klist_t *commands, karg_command_t *cmd, char *name, char *help, karg_command_callback_f callback);



karg_command_t *karg_find_command(klist_t *commands, char *arg);
karg_option_t *karg_find_option(klist_t *options, char short_option);
karg_getopt_option_def *karg_build_getopts_allocs(karg_t *args);
int karg_parse_as_getopts(karg_t *args, karg_getopt_option_def *def, int argc, char **argv);
int karg_add_error(karg_error_t *errors, char *fmt, ...);

/**
 * @brief Calls getopts until non-options encountered.  Permutes argv to move
 * non opts to end of array.
 *
 * @param args p_args:...
 * @param def  p_def:...
 * @param argc p_argc:...
 * @param argv p_argv:...
 * @return int
 */
int karg_parse_args(karg_t *args, karg_getopt_option_def *def, int argc, char **argv);

/**
 * @brief If a callback was provided, this is invoked after option is processed and assigned it's value if one is present
 *
 * @param args p_args:...
 * @param opt p_opt:...
 * @param argv p_argv:...
 * @return int | 0=SUCCESS, !=0=FAILURE
 */
int karg_option_handle_value(karg_t *args, karg_option_t *opt, char *argv);

/**
 * @brief Looks for missing required options in global and if a command was found, the same for that command's options.
 *
 * @param args p_args:...
 * @return int | 0 on SUCCESS, !=0 on ERROR
 */
int karg_validate_args(karg_t *args);

/**
 * @brief Looks for missing required options in given list
 *
 * @param args p_args:...
 * @param options p_options:...
 * @return int | 0 on SUCCESS, !=0 on ERROR
 */
int karg_validate_opts(karg_t *args, klist_t *options);



void karg_print_help(karg_t *arg);
void karg_print_commands_help(int padding, klist_t *commands);
void karg_print_options_help(int padding, klist_t *options);
#endif
